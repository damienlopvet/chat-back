const express = require("express");
const app = express();
http = require("http");
const cors = require("cors");
const { Server } = require("socket.io"); // Add this

app.use(cors()); // Add cors middleware

const server = http.createServer(app); // Add this
console.log("environment", process.env.NODE_ENV);
const corsOrigin = process.env.NODE_ENV === "production" ? "http://vpc.lopvet-damien.com" : "http://localhost:3000";

// Create an io server and allow for CORS from http://localhost:3000 with GET and POST methods
const io = new Server(server, {
	cors: {
		origin: `${corsOrigin}`,
		methods: ["GET", "POST"],
	},
});
var userList = {};
// Listen for when the client connects via socket.io-client
io.on("connection", (socket) => {
async function registerUser(user) {
	userList[socket.id] = user;
}
	async function sendUserList(room) {
	let roomUsersIds = await io
	.in(room)
	.fetchSockets()
	.then((users) => users.map((user) => user.id));
	//retreive userName from userList based on roomusers
	console.log("roomUsersIds", roomUsersIds);
	let roomUsers = roomUsersIds.map((id) => userList[id]);
	console.log("roomUsers", roomUsers);

	console.log("userList", userList);
	console.log(`roomUsers`, roomUsers);
	io.to(`${room}`).emit("userList", roomUsers);
	}

	socket.on("join-room", async (data) => {
	const { room, user } = data; // Data sent from client when join_room event emitted
	await socket.join(room);
		await registerUser(user);
		await sendUserList(room);
		socket.to(room).emit("joined-room", { user });
		
	});

	socket.on("new-message", (data) => {
		const { room, messageObject } = data;
		//get local time 24h format

		socket.to(`${room}`).emit("new-message", messageObject);
	});

	socket.on("typing", (data) => {
		const { room, user } = data; // Data sent from client when typing event emitted
		socket.to(`${room}`).emit("typing", {user});
	});

	socket.on("stopTyping", (data) => {
		const { room, user } = data; // Data sent from client when stopTyping event emitted
		socket.to(`${room}`).emit("stopTyping", {user});
	});

	socket.on("disconnection", (data) => {
		const { user, room } = data;
		console.log({...data}, "------------------------------disconnection event------------------------------");
		socket.to(`${room}`).emit("user-disconnected", {user: user});
		// Remove the user from the userList
		delete userList[socket.id];
		socket.leave(room);
		sendUserList(room);
		console.log("userListafter deletion", userList);
	});
});

server.listen(5000, () => console.log("Server is listenning on port 3000"));
