#!/bin/bash

cd work/chat-back

echo "Stopping containers..."

docker compose -f docker-compose.prod.yaml stop

echo "pulling repository..."

git pull

echo "Building containers..."

docker compose -f docker-compose.prod.yaml up --build -d

echo "Deployment completed successfully!"
# Exit the script
exit 0
