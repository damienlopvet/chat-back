#!/usr/bin/env sh

# abort on errors
set -e

git add .

read -p "What is your commit message ? "
if [[ $REPLY ]]
then
git commit -m " $REPLY "

else

echo "No commit message provided, action aborted"

fi

git push

# Just run : bash transfert.sh
