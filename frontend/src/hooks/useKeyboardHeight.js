'use client'

import { useState, useEffect } from "react";
import  debounce  from "lodash.debounce";

export default function useKeyboardHeight() {
	const [keyboardHeight, setKeyboardHeight] = useState(0);

	useEffect(() => {
		const handleResize = debounce(() => {
			if (window.visualViewport) {
				const _keyboardHeight = window.innerHeight - window.visualViewport.height;
				setKeyboardHeight(_keyboardHeight);
				// ---------------------------test----------------------------
				document.body.style.height = `${window.visualViewport.height}px`;
				document.getElementById("bodyheight").innerHTML = `body is :${document.body.offsetHeight}px`;
				// ---------------------------test----------------------------
			}
		}, 100);
		window.addEventListener("resize", handleResize);
		return () => {
			window.removeEventListener("resize", handleResize);
			handleResize.cancel();
		};
	}, []);

	return keyboardHeight;
}


