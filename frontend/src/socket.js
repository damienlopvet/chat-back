"use client";

import { io } from "socket.io-client";

const socketUrl = process.env.NODE_ENV === "production" ? "http://vpc.lopvet-damien.com:5000" : "http://localhost:5000";

const socket = io.connect(`${socketUrl}`); 
console.log("environment", process.env.NODE_ENV, "socket connected to :",socketUrl  );
export default socket;
