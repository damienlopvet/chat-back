"use client";

import { useEffect, useState } from "react";
import socket from "@/socket";


export default function Home() {
	const [isConnected, setIsConnected] = useState(false);
	const [transport, setTransport] = useState("N/A");

	useEffect(() => {
		console.log("connecting...");
		if (socket && socket.connected) {
			onConnect();
			console.log("connected");
		}

		function onConnect() {
			setIsConnected(true);
			setTransport(socket.io.engine.transport.name);

			socket.io.engine.on("upgrade", (transport) => {
				setTransport(transport.name);
			});
		}

		function onDisconnect() {
			setIsConnected(false);
			setTransport("N/A");
		}

		socket.on("connect", onConnect);
		socket.on("disconnect", onDisconnect);
		socket.on("joined-room", (data) => console.log(data));
		socket.on("userConnected", (data) => console.log(` User with ID ${data} is connected`));

		return () => {
			socket.off("connect", onConnect);
			socket.off("disconnect", onDisconnect);
		};
	}, [socket]);

	const joinRoom = () => {
		let room = "roomtest";
		let user = "user1";
		if(room !== "" && user !== ""){socket.emit("join-room", { room, user });}
		console.log("joining room...");
	};

	return (
		<div className="h-screen">
			<p>Status: {isConnected ? "connected" : "disconnected"}</p>
			<p>Transport: {transport}</p>
			<button onClick={joinRoom} type="button" aria-label="join room button" className="rounded-md bg-blue-500 px-4 py-2 text-white">Join ROooo°°°m</button>
		</div>
	);
}
