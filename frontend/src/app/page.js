import React from "react";

const getBackendResponse = async () => {
  const res = await fetch("http://backend:5000");

  const response = await res.json();
  return response.message;

};
export const revalidate = 0;
const page = async() => {
	const resTest = await getBackendResponse();

	return (
		<section className="h-screen">
			<div>Hello, here is the response :</div>
			<div>{resTest && resTest}</div>
		</section>
	);
};

export default page;
