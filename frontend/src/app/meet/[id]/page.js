"use client";
import React, { useEffect, useState, useRef } from "react";
import socket from "@/socket";
import Cookies from "js-cookie";
import Spinloader from "@/components/spinloader";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import useKeyboardHeight from "@/hooks/useKeyboardHeight";

//icons
import { IoMdSend } from "react-icons/io";

const UsernameForm = ({ name, setName, handleUsername }) => {
	return (
		<div className='absolute z-10 inset-0 backdrop-blur-[1px] h-screen grid place-items-center'>
			<form onSubmit={handleUsername} className='max-w-sm flex flex-col justify-center gap-4 p-4 w-full bg-blue-400'>
				<Label htmlFor='username'>Username</Label>
				<Input id='username' type='text' className='bg-blue-300' placeholder='Enter your name' value={name} onChange={(e) => setName(e.target.value)} />
				<Button type='submit'>Submit</Button>
			</form>
		</div>
	);
};

const UserListComponent = ({ userList }) => {
	console.log("userList : ", userList);
	return (
		<p id='userList' className='top-0 py-2 w-full z-10 bg-[#F1F1F1]/80 backdrop-blur sticky'>
			{userList.map((user) => {
				return (
					<span key={user} className='first:ml-5 py-1 px-2 '>
						{user}&nbsp;
					</span>
				);
			})}
		</p>
	);
};

const NewMessageForm = ({ newMessage, setNewMessage, sendNewMessage, room, user, userTyping }) => {

	const emitTyping = (isTyping) => {
		let data = {
			room: room,
			user: user,
		};
		if (isTyping) {
			socket.emit("typing", data);
		} else {
			socket.emit("stopTyping", data);
		}
	};

	useEffect(() => {
		emitTyping(newMessage.length > 0);
	}, [newMessage]);

	return (
		<div id='newMessage_form' className={`w-full fixed bottom-0`}>
			<div className='w-full flex flex-row absolute -top-[0.8rem]'>
				{userTyping.length > 0 &&
					userTyping.map((user, index) => (
						<p key={index} className='text-white text-[8px] text-center capitalize first:ml-2'>
							{user}&nbsp;
						</p>
					))}
				{userTyping.length > 0 && <span className='text-white text-[8px]'>{userTyping.length > 1 ? " are " : " is "} typing</span>}
			</div>
			<form onSubmit={sendNewMessage} className='flex flex-row justify-center gap-2 py-2 px-4 w-full bg-[#F1F1F1]'>
				<Label htmlFor='message' className='hidden'>
					Message
				</Label>
				<Input id='message' type='text' className='bg-slate-300 rounded-2xl' placeholder='Enter your message' value={newMessage} onChange={(e) => setNewMessage(e.target.value)} />
				<Button type='submit' className={`transition-all duration-300 bg-green-400 dark:!bg-[#005046] ${newMessage ? "w-12 px-3" : "w-0 px-0"}`}>
					<IoMdSend className='w-7 h-7' />
				</Button>
			</form>
		</div>
	);
};

const MessageListComponent = ({ messageList, userName }) => {
	const bottomRef = useRef(null);

useEffect(() => {
	//each time there is a new message, scroll to the bottom
	const scrollToBottom = () => {
	if (bottomRef.current) {
		bottomRef.current.scrollIntoView({ behavior: "smooth" });
		
	}
		};
		 scrollToBottom(); // Also scroll to bottom when a new message arrives

	}, [messageList]);

	return (
		<div id='message_list' className={`flex-grow flex flex-col gap-4 overflow-y-auto relative pt- pb-20`}>
			{messageList.map((message, index) => {
				return (
					<div
						key={index}
						className={`flex flex-col gap-2 border rounded-lg w-1/2 p-2 first:mt-8 ${message.userName === userName ? "ml-auto mr-8 bg-[#E2FFD4] dark:!bg-[#005046]" : "ml-8 bg-slate-400"}`}>
						{message.userName !== userName && <p className='text-black dark:text-white text-xs capitalize'>{message.userName}</p>}
						<p className='text-black dark:text-white'>{message.message}</p>
						<p className='ml-auto text-black dark:text-white text-xs'>{message.time} </p>
					</div>
				);
			})}
			<div className='absolute inset-0 grid place-items-center'>
			</div>
			<div ref={bottomRef} className='absolute bottom-0'></div>
		</div>
	);
};

const Page = ({ params: { id } }) => {
	const [isConnected, setIsConnected] = useState(false);
	const [transport, setTransport] = useState("N/A");
	const [pageLoading, setPageLoading] = useState(true);
	const [userName, setUserName] = useState("");
	const [name, setName] = useState("");
	const [botMessage, setBotMessage] = useState("");
	const [newMessage, setNewMessage] = useState("");
	const [messageList, setMessageList] = useState([]);
	const [userList, setUserList] = useState([]);
	const [userTyping, setUserTyping] = useState([]);
	const userNameRef = useRef("");
	// // const keyboardHeight = useKeyboardHeight();
const keyboardHeight = '100vh'

	useEffect(() => {
		const _userName = Cookies.get("userName");
		if (_userName) {
			setUserName(_userName);
			userNameRef.current = _userName;
			joinRoom(_userName);
		}
		setPageLoading(false);
	}, []);

	
	//socket logic
	useEffect(() => {
		console.log("connecting...");

		const onConnect = () => {
			setIsConnected(true);
			setTransport(socket.io.engine.transport.name);

			socket.io.engine.on("upgrade", (transport) => {
				setTransport(transport.name);
			});
		};

		const onDisconnect = () => {
			setIsConnected(false);
			setTransport("N/A");
		};

		const handleMessage = (data) => {

			setMessageList((prevMessages) => [...prevMessages, data]);


		let time = getTime();
		data.time = time;
			console.log("new message from server: ", data);
		};

		const handleUserJoinedRoom = (data) => {
			const { user } = data;
			setBotMessage(`${user} is connected`);
			setTimeout(() => {
				setBotMessage("");
			}, 3000);
		};

		const handleUserListUpdate = (data) => {
			setUserList(data);
		};

		const handleUserTyping = (data) => {
			setUserTyping((prevUsers) => {
				if (!prevUsers.includes(data.user)) {
					return [...prevUsers, data.user];
				}
				return prevUsers;
			});
		};

		const handleUserStopTyping = (data) => {
			setUserTyping((prev) => prev.filter((user) => user !== data.user));
		};
		const handleUserDisconnected = (data) => {
			console.log(`disconnected`, { ...data });
			const { user } = data;
			setBotMessage(`${user} is disconnected`);
			setTimeout(() => {
				setBotMessage("");
			}, 3000);
		};

		const handleBeforeUnload = () => {
			socket.emit("disconnection", { user: userNameRef.current, room: id });
		};

		window.addEventListener("beforeunload", handleBeforeUnload);

		socket.on("connect", onConnect);
		socket.on("disconnect", onDisconnect);
		socket.on("joined-room", handleUserJoinedRoom);
		socket.on("userList", handleUserListUpdate);

		socket.on("user-disconnected", handleUserDisconnected);
		socket.on("new-message", handleMessage);
		socket.on("typing", handleUserTyping);
		socket.on("stopTyping", handleUserStopTyping);

		return () => {
			handleBeforeUnload();
			window.removeEventListener("beforeunload", handleBeforeUnload);
			socket.off("connect", onConnect);
			socket.off("disconnect", onDisconnect);
			socket.off("joined-room", handleUserJoinedRoom);
			socket.off("userList", handleUserListUpdate);
			socket.off("userConnected");
			socket.off("user-disconnected", handleUserDisconnected);
			socket.off("new-message", handleMessage);
			socket.off("typing", handleUserTyping);
			socket.off("stopTyping", handleUserStopTyping);
		};
	}, []);

	const joinRoom = (_username) => {
		let room = id;
		let user = _username;
		if (room !== "" && user !== "") {
			socket.emit("join-room", { room, user });
		} else {
			console.log("room or user not found");
		}
	};

	const handleUsername = (e) => {
		e.preventDefault();
		const userName = e.target[0].value;
		Cookies.set("userName", userName);
		setUserName(userName);
		userNameRef.current = userName;
		joinRoom(userName);
	};

	const getTime = () => {
		const date = new Date();
		let time = date.toLocaleTimeString("fr-FR", {
			hour12: false,
			hour: "2-digit",
			minute: "2-digit",
		});
		return time;
	}
	const sendNewMessage = (e) => {
		e.preventDefault();
		if (!newMessage) return;
		let room = id;
		let time =getTime();
		let messageObject = {
			userName,
			message: newMessage,
			time: time,
		};

		socket.emit("new-message", { messageObject, room });
		setMessageList((prevMessages) => [...prevMessages, messageObject]);
		setNewMessage("");
	};

	return (
		<div id='chat_page' className={`bg-[#EFE9E0] min-h-[100dvh] flex flex-col relative`}>
			{pageLoading && <Spinloader />}
			{!userName && <UsernameForm name={name} setName={setName} handleUsername={handleUsername} />}
			{botMessage && (
				<div className='text-5xl fadeOut absolute inset-0 grid place-items-center'>
					<p>{botMessage}</p>
				</div>
			)}

			{userList.length > 0 && <UserListComponent userList={userList} />}
			{messageList && <MessageListComponent messageList={messageList} userName={userName} />}

			{userName && <NewMessageForm newMessage={newMessage} setNewMessage={setNewMessage} sendNewMessage={sendNewMessage} room={id} user={userName} userTyping={userTyping} />}
		</div>
	);

};

export default Page;
