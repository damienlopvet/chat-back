"use client";
import { useState } from "react";

const Page = () => {
	const [vitesse, setVitesse] = useState(0);
	const [routeSeche, setRouteSeche] = useState(false);
	const [distanceFreinage, setDistanceFreinage] = useState(0);
	const [distanceReaction, setDistanceReaction] = useState(0);
    const [distanceDarret, setDistanceDarret] = useState(0);
    const [tempsDeReaction, setTempsDeReaction] = useState(0);
    const distancedeReaction = () => {
        if (!tempsDeReaction || tempsDeReaction < 0) {
            alert("compléter vitesse et temps de réaction");
            return
        }
        
        let base = vitesse / 3.6;
        let res = base * tempsDeReaction
		setDistanceReaction(res);
		return res;
	};

	const distancedeFreinage = (val, bol) => {
		let res;
		if (bol) {
			res = ((val * val) / 100) * (3 / 5);
		} else {
			res = (val * val) / 100;
		}
		setDistanceFreinage(res);
		return res;
	};
	const calculdistanceDarret = (e) => {
        e.preventDefault();
		let Reaction = distancedeReaction(vitesse, routeSeche);
		let Freinage = distancedeFreinage(vitesse, routeSeche);
		let res = Reaction + Freinage
		setDistanceDarret(res);
		return;
	};

	return (
		<>
			<section className='box-border p-6'>
				<h1 className='text-3xl text-center pb-6'>Distance de freinage</h1>
				<form action='' onSubmit={calculdistanceDarret} className='flex flex-col gap-4'>
					<label htmlFor='vitesse' className=' flex flex-row items-baseline gap-4'>
						vitesse
						<input type='number' id='vitesse' className='border rounded-lg p-2 w-20' onChange={(e) => setVitesse(e.target.value)} />
						<span>Km/h </span>
					</label>
					<label htmlFor='routeSeche' className=' flex flex-row items-baseline gap-4'>
						route sèche
						<input type='checkbox' id='routeSeche' className='border rounded-lg p-2' onChange={(e) => setRouteSeche(e.target.checked)} />
					</label>
					<label htmlFor='tempsDeReaction' className=' flex flex-row items-baseline gap-4'>
						{" "}
						temps de réaction
						<input type='number' id='tempsDeReaction' className='border rounded-lg p-2 w-20' onChange={(e) => setTempsDeReaction(e.target.value)} />
						<span>s</span>
					</label>
					<button type='submit' className='border rounded-lg p-2'>
						Calculer
					</button>
				</form>
				<h2 className='text-2xl py-6'>resultat </h2>
				<p>distance de Reaction = {distanceReaction.toFixed(1)} m</p>
				<p>distance de Freinage = {distanceFreinage.toFixed(1)} m</p>
				<p>distance de Darret = {distanceDarret.toFixed(1)} m</p>
			</section>
		</>
	);
};

export default Page;
