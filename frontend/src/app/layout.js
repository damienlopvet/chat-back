import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });
export const viewport = {
width:'device-width', initialScale:1, maximumScale:1, interactiveWidget:'resizes-content'
}
export const metadata = {
	title: "Create Nexts App",
	description: "Next.js + WebRTC + WebSockets + Web Audio API",

};

export default function RootLayout({ children }) {
	return (
		<>
			<html lang='fr' className=''>
		
				<body className={`${inter.className} min-h-full h-full`}>{children}</body>
			</html>
		</>
	);
}
