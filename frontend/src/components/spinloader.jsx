import React from "react";

const Spinloader = () => {
	return (
		<div className='absolute z-10 inset-0 grid place-items-center bg-black/70 backdrop-blur-sm'>
			<div className='border-t-transparent border-solid animate-spin  rounded-full border-blue-400 border-8 h-12 w-12'></div>
		</div>
	);
};

export default Spinloader;
