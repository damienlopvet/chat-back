import { Button } from "@/components/ui/button";
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import Cookies from "js-cookie";


export default function getUsernameBox({name, setName, setUsername}) {
	const handleUsername = (e) => {
		e.preventDefault();
		const userName = name;
		Cookies.set("userName", userName);
		setUsername(name);
	};
	return (
		<Dialog>
			<DialogTrigger asChild>
				<Button variant='outline'>Edit Profile</Button>
			</DialogTrigger>
			<DialogContent className='sm:max-w-[425px]'>
				<DialogHeader>
					<DialogTitle>Edit profile</DialogTitle>
					<DialogDescription>Make changes to your profile here. Click save when you're done.</DialogDescription>
				</DialogHeader>
				<form className='grid gap-4 py-4' onSubmit={handleUsername}>
					<div className='grid grid-cols-4 items-center gap-4'>
						<Label htmlFor='username' className='text-right'>
							Username
						</Label>
						<Input id='username' value={name} className='col-span-3' onchange={(e) => setName(e.target.value)}/>
					</div>
				</form>
				<DialogFooter>
					<button type='submit'>Save changes</button>
				</DialogFooter>
			</DialogContent>
		</Dialog>
	);
}
